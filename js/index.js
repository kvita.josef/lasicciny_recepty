var enlarged_before;

// make and place all notes
var makeNotes = function () {
    // make a single note
    var makeNote = function(index) {
        var img_url = `img/food/${parseInt(Math.random() * 10, 10)}.png`;
        var img_url1 = `img/food/${parseInt(Math.random() * 10, 10)}.png`;
        var img_url2 = `img/food/${parseInt(Math.random() * 10, 10)}.png`;


        var numberOfStars = Math.max(parseInt(Math.random() * 5, 10), 1);
        var numberOfChilli = parseInt(Math.random() * 5, 10);

        return `
            <div class="note-container">
                <div class="note">
                    <button class="button"><i class="material-icons">edit</i></button>
                    <section class="text">
                        <h2>Recept číslo ${index + 3}</h2>
                        <section class="properties">
                            <section class="symbols">
                                <section class="stars">
                                    ${'<img src="img/other/star.png">'.repeat(numberOfStars)}
                                </section>
                                <section class="chilli">
                                    ${'<img src="img/other/chilli.png">'.repeat(numberOfChilli)}
                                </section>
                            </section>
                            <section class="ingredients">
                                <h3>Suroviny:</h3>
                                <ul>
                                    <li>surovina číslo 1</li>
                                    <li>surovina číslo 2</li>
                                    <li>surovina číslo 3</li>
                                    <li>dlouhááááárwerwerwerwerwerwerwerwerwerwerwerwerweááááá surovinaaaaaaaaa číslo 4</li>
                                    <li>surovina číslo 5</li>
                                    <li>surovina číslo 6</li>
                                </ul>
                            </section>
                        </section>
                        <section class="description">
                            <h3>Příprava:</h3>
                            <p>Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.
                               Opravdu dlouhý text o tomto výborném receptu.</p>
                        </section>
                    </section>
                    <section class="images">
                        <img src=${img_url} class="image main">
                        <img src=${img_url1} class="image">
                        <img src=${img_url2} class="image">
                    <section>
                </div>
            </div>
            `;
    };

    // add new notes
    for (var i = 0; i <= 250; i++) {
        var note = makeNote(i);
        $(".notes").append(note);
    }
};

// enlarge note
var enlargeNote = function (note) {
    note.addClass("enlarged");
    $("#dimmer").addClass("dimmed");
    $("#header").addClass("dimmed");
    $(".notes").masonry( "option", { transitionDuration: 0 });
    enlarged_before = note;
};

// shrink note
var shrinkNote = function(event) {
    if (enlarged_before) {
        $("#dimmer").removeClass("dimmed");
        $("#header").removeClass("dimmed");
        enlarged_before.removeClass("enlarged");
        // position the notes once more and then enable the duration
        notes = $(".notes");
        notes.masonry();
        notes.masonry( "option", { transitionDuration: "0.4s" });
    }
};

// make object editable, add .editable class
var toggleEditable = function (objs) {
    for (index = 0; index < objs.length; ++index) {
         var obj = objs[index];
         var isEditable = obj.is('.editable')
         obj.prop('contenteditable',!isEditable).toggleClass('editable');
    }
};


// do these actions when the document is ready
$(document).ready(function() {
    // make the notes
    makeNotes();

    // note onclick handler to enlarge
    $(".note").click(function () {
        history.replaceState(null, null, location.href);
        enlargeNote($(this));
    });

    // shrink the note when clicked on the dimmer or header
    $("#dimmer").click(shrinkNote);
    $("#header").click(function() {
        if ($(this).hasClass("dimmed")) {
             shrinkNote();
        }
    });

    // keyup handler for multiple purposes
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            shrinkNote();
        }
    });

    // position the notes after the whole window has been loaded
    $(window).bind("load", function() {
        $(".notes").masonry();
        $("html").css({ visibility: "visible"});
    });

    // making the back button shrink the note
    var working = true;
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        if ($("#dimmer").hasClass("dimmed")) {
             shrinkNote();
             working = false;
             history.go(1);
        }
        else {
            if (working) {
                 //history.go(-1)
                 history.back()
            }
            else {
                 working = true;
            }
        }
    };

//    $(window).resize(function () {
//        var note = $(".note.enlarged");
//        var img = note.find(".img-main");
//
//        if (img.width() > img.height()) {
//            img.css({max-width: 'none', max-height: 'none', width: "80vw"});
//        }
//    });


//    $(".button").click(function() {
//        var b = $(this)
//        if (b.id == "edit") {
//            b.toggleClass("hidden");
//            $("#save").show()
//        }
//        else if (b.id == "save") {
//            b.toggleClass("hidden");
//            $("#edit").show()
//        }
//
//        toggleEditable([$('p'), $('h2')]);
//    });
});






